Started according to GitLab instructions on (https://gitlab.com/parez/flapsit01-web)  
 incl. "touch README.md", etc.

Followed https://webpack.js.org/guides/getting-started/ :  
Installing WEBPACK locally, in terminal
```
C:\Syncthing\DEVS\flapsit01-web>
npm init -y
npm install --save-dev webpack
npm install --save lodash

# .\node_modules\.bin\webpack src\index.js dist\bundle.js
# .\node_modules\.bin\webpack --config webpack.config.js # ->
.\node_modules\.bin\webpack
```

Followed https://vuejs.org/v2/guide/installation.html#NPM :  
```
npm install vue
#`-- vue@2.4.2
npm install --global vue-cli

cd ..
C:\Syncthing\DEVS>vue init webpack flapsit01-web

? Target directory exists. Continue? Yes
? Project name flapsit01-web
? Project description A Vue.js project
? Author Paul Reznicek <pareznik@gmail.com>
? Vue build standalone
? Install vue-router? Yes
? Use ESLint to lint your code? Yes
? Pick an ESLint preset Standard
? Setup unit tests with Karma + Mocha? Yes
? Setup e2e tests with Nightwatch? Yes
   vue-cli · Generated "flapsit01-web".

   To get started:
     cd flapsit01-web
     npm install
     npm run dev
   Documentation can be found at https://vuejs-templates.github.io/webpack
```

working through: https://medium.com/tutorialsxl/vue-js-with-visual-studio-code-getting-started-3ef9829eb13e  
follow: https://medium.com/@amrswalha/latest  

